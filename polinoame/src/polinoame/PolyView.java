package polinoame;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.*;


public class PolyView extends JFrame {
	private JTextField p1 = new JTextField(30);
	private JTextField p2 = new JTextField(30);
	private JTextField poly1 = new JTextField(30);
	private JTextField poly2 = new JTextField(30);
	private JTextField result = new JTextField(30);
	private JTextField rest = new JTextField(30);
	
	private JButton der_p1 = new JButton("der");
	private JButton der_p2 = new JButton("der");
	private JButton int_p1 = new JButton("int");
	private JButton int_p2 = new JButton("int");
	private JButton add = new JButton("+");
	private JButton sub = new JButton("-");
	private JButton mul = new JButton("*");
	private JButton div = new JButton("/");
	
	
	public PolyView(){
		JPanel content = new JPanel();
		content.setLayout(new BorderLayout());
		
		
		JLabel l1 = new JLabel("P1:");
		JLabel l2 = new JLabel("P2");
		poly1.setEditable(false);
		poly2.setEditable(false);
		result.setEditable(false);
		rest.setEditable(false);
		JPanel p1Op = new JPanel();
		p1Op.setLayout(new GridLayout(1,2));
		p1Op.add(der_p1);
		p1Op.add(int_p1);
		JPanel p2Op = new JPanel();
		p2Op.setLayout(new GridLayout(1,2));
		
	
		p2Op.add(der_p2);
		p2Op.add(int_p2);
		
		JPanel intr = new JPanel();
		intr.setLayout(new GridLayout(4,2));
		intr.add(l1);
		intr.add(l2);
		intr.add(p1);
		intr.add(p2);
		intr.add(poly1);
		intr.add(poly2);
		intr.add(p1Op);
		intr.add(p2Op);
		
		JPanel res = new JPanel();
		res.setLayout(new GridLayout(1,2));
		JPanel ops = new JPanel();
		ops.setLayout(new GridLayout(2,4));
		ops.add(new JPanel());
		ops.add(add);
		ops.add(sub);
		ops.add(new JPanel());
		ops.add(new JPanel());
		ops.add(mul);
		ops.add(div);
		ops.add(new JPanel());
		JPanel resView = new JPanel();
		resView.setLayout(new GridLayout(4,1));
		resView.add(new JLabel("Result:"));
		resView.add(result);
		resView.add(new JLabel("Rest:"));
		resView.add(rest);
		res.add(ops);
		res.add(resView);
		
		intr.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		res.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		
		content.add(intr, BorderLayout.PAGE_START);
		content.add(res,BorderLayout.PAGE_END);
		
		
	
		
		this.setContentPane(content);
		this.setVisible(true);
		//this.setSize(600, 250);
		this.pack();
		this.setTitle("Polynom Processor");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
				
				
	}
	//adaugare listenere primite ca parametru pe obiectele din view(butoane,textfield-uri etc)
	public void addUserInputListenerP1(KeyListener l) {
		p1.addKeyListener(l);
	}
	
	public void addUserInputListenerP2(KeyListener l) {
		p2.addKeyListener(l);
	}
	
	public void addSumListener(ActionListener l) {
		add.addActionListener(l);
	}
	public void addSubListener(ActionListener l) {
		sub.addActionListener(l);
	}
	public void addMulListener(ActionListener l) {
		mul.addActionListener(l);
	}
	public void addDivListener(ActionListener l) {
		div.addActionListener(l);
	}
	public void addDerP1Listener(ActionListener l) {
		der_p1.addActionListener(l);
	}
	public void addDerP2Listener(ActionListener l) {
		der_p2.addActionListener(l);
	}
	public void addIntP1Listener(ActionListener l) {
		int_p1.addActionListener(l);
	}
	public void addIntP2Listener(ActionListener l) {
		int_p2.addActionListener(l);
	}
	
	
	public String getP1(){
		return p1.getText().toString();
	}
	
	public String getP2(){
		return p2.getText().toString();
	}
	
	public JTextField getPoly1(){
		return poly1;
	}
	
	public JTextField getPoly2(){
		return poly2;
	}
	
	public JTextField getResult(){
		return result;
	}
	
	public JTextField getRest(){
		return rest;
	}
	
	public static void main(String[] args){
		PolyView process = new PolyView();
	}
}
