package polinoame;

public class Monom implements Comparable<Monom>{
	private double coef; //coeficientul monomului
	private int power; //exponentul monomului
	
	public Monom(double coef,int power){ //consturctor monom
		if( coef != 0){
			this.coef = coef;
			this.power = power;
		}
		else{
			this.coef = 0;
			this.power = 0;
		}
	}
	
	public Monom getMonom(){ //getter monom
		return this;
	}
	
	public double getCoef(){//getter coeficient)
		return this.coef;
	}
	public int getPower(){ //getter  exponent
		return this.power;
	}
	public void setCoef(double coef){ //setter coeficient
		this.coef = coef; 
	}
	public void setPower(int power){ //setter exponent
		this.power = power;
	}
	
	public Monom add(Monom other) throws DifGrad{ //adunare doua monoame
		Monom result;
		if(this.power == other.power) //nu pot aduna doua monoame daca nu au acelasi exponent
			result =new Monom(this.coef+other.coef,this.power);
		else
			throw new DifGrad();
		
		return (result.coef!=0)? result : new Monom(0,0); //returnez rezultatul adunarii sau 0
														// in caz ca nu se pot aduna
	}
	
	public Monom sub(Monom other) throws DifGrad{ //scad doua monoame
		Monom result;
		if(this.power == other.power) //pot scadea doua monoame doar daca au acelasi exponent
			result = new Monom(this.coef - other.coef,this.power);//rezultat scadere
		else throw new DifGrad();
		return (result.coef!=0)? result : new Monom(0,0); //returnez rezultat sau 0 in caz ca
														 // nu se pot scadea
	}
	
	public Monom mul(Monom other){//inmultire monoame
		Monom result; 
		result = new Monom(this.coef * other.coef,this.power + other.power); //rezultat inmultire
		return (result.coef!=0)? result : new Monom(0,0); //returnez rezultatul inmultirii sau 0
														//daca coeficientul rezultatului e 0
	}
	
	public Monom div(Monom other){//impartire monoame
		if(other.coef != 0){ //nu pot imparti la 0
			Monom result;
			result = new Monom(this.coef / other.coef,this.power - other.power);//rezultat  impartire
			return (result.coef!=0)? result : new Monom(0,0); //returnez rezultatul sau 0
		}
		else throw new ArithmeticException("0 division");

	}
	
	public Monom der(){ //derivare monom
		if(this.power > 0 ) //daca exponentul e 0 returnez direct 0
			return new Monom(this.coef * power,this.power -1);//returnez monomul derivat
		else
			return new Monom(0,0);
	}
	
	public Monom integrate(){//integrare monom
		if(this.coef == 0) //daca coeficientul este 0 returnez 0
			return new Monom(0,0);
		else
			return new Monom(this.coef / (this.power + 1),this.power + 1);//rezultat integrare
	}
	 
	public int compareTo(Monom other){ //suprascriu functia compare pt a putea sorta monoamele
		if(this.power > other.power) // in lista din clasa Polynom intr-o ordine convenabila
			return -1;
		else if(this.power == other.power)
			return 0;
		else
			return 1;
	}
	
	public String toString(){ // suprascriu metoda toString pt a afisa o reprezentare 
		String coeficient = ""; // "citibila" a monomului
		String variabila = "";
		String exp = "";
		if(coef == Math.floor(coef)){
			if((coef==1 ) && power != 0)
				coeficient+="";
			else if((coef== -1 ) && power != 0)
				coeficient+="-";
			else
				coeficient+=(int)coef;	
		}
		else
			coeficient+=String.format("%.2f", coef);
		if(power != 0 ){
			if(power == 1){
				variabila += "x";
			}
			else{
				variabila += "x^";
				exp	+= power;
			}
		}
		
		return coeficient+variabila+exp;
		//return (coef == Math.floor(coef))? (int)coef+((this.power!=0)?"x^"+ power:"") : 
			//			String.format("%.2f", coef)+((this.power!=0)?"x^"+ power:"") ;
		//return coef+"x^"+power;
	}
	
	
	public static void main(String[] args){
		Monom m1 = new Monom(1,2);
		Monom m2 = new Monom(1,1);
		try{
			System.out.println("SUM="+m1.add(m2));
			System.out.println("SUB="+m1.sub(m2));
			
		}
		catch(DifGrad e){
			System.out.println(e.getMessage());
		}
		System.out.println("PROD="+m1.mul(m2));
		try{
			System.out.println("DIV="+m1.div(m2));
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		System.out.println("DERIVATE="+m1.der());
		System.out.println("INTEGRALA="+m1.integrate());
		System.out.println(m1.compareTo(m2));
	}
}
