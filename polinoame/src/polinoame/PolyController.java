package polinoame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;



public class PolyController {
	
	private PolyView p_view;
	private PolyModel p_model;
	
	public PolyController(PolyView view,PolyModel model){
		this.p_view = view;
		this.p_model = model;
		p_view.addUserInputListenerP1(new UserInputListenerP1()); //listener pt tastare p1
		p_view.addUserInputListenerP2(new UserInputListenerP2()); //listener pt tastare p2
		p_view.addSumListener(new SumListener()); //listener buton suma
		p_view.addSubListener(new SubListener()); //etc..listenere pt restul butoanelor din view
		p_view.addMulListener(new MulListener());
		p_view.addDivListener(new DivListener());
		p_view.addDerP1Listener(new DerP1Listener());
		p_view.addDerP2Listener(new DerP2Listener());
		p_view.addIntP1Listener(new IntP1Listener());
		p_view.addIntP2Listener(new IntP2Listener());

	}
	
	class IntP2Listener implements ActionListener {  //executa operatia de integrare a lui p2
		public void actionPerformed(ActionEvent e) { //seteaza in view rezultatul
			p_model.intP2();
			p_view.getResult().setText(p_model.getResult().toString());
			p_view.getRest().setText("");

		}
	}
	class IntP1Listener implements ActionListener { //integreaza p1
		public void actionPerformed(ActionEvent e) {//seteaza in view rezzultatul
			p_model.intP1();
			p_view.getResult().setText(p_model.getResult().toString());
			p_view.getRest().setText("");

		}
	}
	class DerP2Listener implements ActionListener { //deriveaza p2
		public void actionPerformed(ActionEvent e) { //seteaza in view rezultatul
			p_model.derP2();
			p_view.getResult().setText(p_model.getResult().toString());
			p_view.getRest().setText("");

		}
	}
	class DerP1Listener implements ActionListener { //deriveaza p1
		public void actionPerformed(ActionEvent e) { // seteaza in view rezultatul
			p_model.derP1();
			p_view.getResult().setText(p_model.getResult().toString());
			p_view.getRest().setText("");

		}
	}
	class SumListener implements ActionListener {
		public void actionPerformed(ActionEvent e) { //aduna polinoamele
			p_model.add();							//seteaza in view rezultatul
			p_view.getResult().setText(p_model.getResult().toString());
			p_view.getRest().setText("");
		}
	}
	class SubListener implements ActionListener { //scade polinoamele
		public void actionPerformed(ActionEvent e) { // seteaza in view rezultatul
			p_model.sub();
			p_view.getResult().setText(p_model.getResult().toString());
			p_view.getRest().setText("");

		}
	}
	class MulListener implements ActionListener { //inmulteste polinoamele
		public void actionPerformed(ActionEvent e) { //seteaza in view rezultatul
			p_model.mul();
			p_view.getResult().setText(p_model.getResult().toString());
			p_view.getRest().setText("");
		}
	}
	class DivListener implements ActionListener { //imparte polinoamele
		public void actionPerformed(ActionEvent e) { //seteaza in view rezultatul
			p_model.div();
			p_view.getResult().setText(p_model.getResult().toString());
			p_view.getRest().setText(p_model.getRest().toString());

		}
	}
	class UserInputListenerP1 implements KeyListener { //urmareste inputul userului in 
		public void keyPressed(KeyEvent keyEvent) { //textfield ul pt p1

		}

		public void keyReleased(KeyEvent keyEvent) {
			String poly = p_view.getP1();
			if(poly.length() != 0){
				p_model.resetP1();
				p_model.setP1(poly);
				p_view.getPoly1().setText(p_model.getP1().toString());
			}
			else{
				p_model.resetP1();
				p_view.getPoly1().setText(p_model.getP1().toString());
			}
		}

		public void keyTyped(KeyEvent keyEvent) {

		}

	}
	class UserInputListenerP2 implements KeyListener { //urmareste inputul userului pt p2
		public void keyPressed(KeyEvent keyEvent) {

		}

		public void keyReleased(KeyEvent keyEvent) {
			String poly = p_view.getP2();
			if(poly.length() != 0){
				p_model.resetP2();
				p_model.setP2(poly);
				p_view.getPoly2().setText(p_model.getP2().toString());
			}
			else{
				p_model.resetP2();
				p_view.getPoly2().setText(p_model.getP2().toString());
			}
		}

		public void keyTyped(KeyEvent keyEvent) {

		}

	}
	public static void main(String[] args){
		PolyView view = new PolyView();
		PolyModel model = new PolyModel();
		PolyController controller = new PolyController(view,model);
	}
}
