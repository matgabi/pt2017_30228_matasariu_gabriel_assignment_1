package polinoame;


import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Polynom {

	private List<Monom> poly; //polinomul propriu zis(lista de monoame)

	public Polynom() {
		this.poly = new ArrayList<Monom>();
	}

	public void parsePoly(String polynomial) { //functia de extragere polinom dintr-un string
		if(polynomial.length() != 0){
			String monomialFormat = "([+-]?[\\d]*[\\.]?[\\d]*[a-zA-Z]?\\^?\\d*)", //forma monom
					monomialPartsFormat = "([+-]?[\\d]*[\\.]?[\\d]*)([a-zA-Z]?)\\^?(\\d*)";//coeficient+exponent monom
			Pattern p1 = Pattern.compile(monomialFormat);
			Matcher m1 = p1.matcher(polynomial);
		
			while (!m1.hitEnd()) { //atata timp cat gasesc monoame in string
				m1.find();
				Pattern p2 = Pattern.compile(monomialPartsFormat); //ii caut coeficientul si exp
	
				Matcher m2 = p2.matcher(m1.group());
				if (m2.find()) {
					double coefficient;  //incerc sa iau coeficientul
					try {
						String coef = m2.group(1);
						if (isNumeric(coef)) {
							coefficient = Double.valueOf(coef);
						} else {
							coefficient = Double.valueOf(coef + "1");
						}
					} catch (IllegalStateException e) {
						coefficient = 0.0F;
					}
					int exponent;
					try { //incerc sa extrag exponentul
						String exp = m2.group(3);
						if (isNumeric(exp)) {
							exponent = Integer.valueOf(exp);
						} else {
							if(m2.group(2).equals("x"))
								exponent = 1;
							else
								exponent = 0;
						}
					} catch (IllegalStateException e) {
						exponent = 0;
					}
					Monom m = new Monom(coefficient,exponent); //construiesc monomul decodificat din sttring
					poly.add(m); //adaug monomul in lista de monoame care constituie de fapt polinomul
				}
			}
			
			this.autoFormat(); //apelez functia de auto format
		}
	}
	
	public void autoFormat(){ //functia autoformateaza lista de monoame facand-o "citibila"
		Collections.sort(this.poly); //sortez lista de monoame
		Monom m=new Monom(0,0); 
		for(int i = 0 ; i < this.poly.size()-1 ; i++){ 
			if(this.poly.get(i).getCoef()==0)
				this.poly.set(i,m);
		}
		while(this.poly.contains(m)){
			this.poly.remove(m); //elimin din lista de monoame acele polinoame care sunt de fapt 0,in caz ca le-am introdus
		}
		for(int i = 0 ; i < this.poly.size()-1 ; i++){ //in cazul in care am introdus doua polinoame cu acelasi exponent acestea se vor aduna
			for(int j = i+1 ; j < this.poly.size() ; j++){//pt a ramane in lista doar ceea ce e relevant
				if(this.poly.get(i).getPower() == this.poly.get(j).getPower() &&
						this.poly.get(i).getCoef()!= 0){
					try{
						this.poly.set(i,this.poly.get(i).add(this.poly.get(j)));
						this.poly.set(j, m);
					}
					catch(DifGrad e){
						System.out.println(e.getMessage());
					}
				}
				else if(this.poly.get(i).getPower() > this.poly.get(j).getPower())
					break;
			}
		}
		for(int i = 0 ; i < this.poly.size()-1 ; i++){
			if(this.poly.get(i).getCoef()==0)
				this.poly.set(i,m);
		}
		while(this.poly.contains(m)){
			this.poly.remove(m); //elimin inca o data monoame care sunt de fapt 0 care ar fi puutut rezulta
								//din operatia de adunare a doua monoame cu acelasi exponent
		}
	}

	private int getGrad(){ //returneaza gradul polinomului
		return this.poly.get(0).getPower();
	}
	
	private boolean isNumeric(String str) { //testeaza daca un string primit poate fi interpretat
		return str.matches("[+-]?\\d*\\.?\\d+");//ca o valoare numerica
	}

	public Polynom add(Polynom other){ //aduna doua polinoame
		Polynom result=new Polynom();
		result.poly.addAll(this.poly);
		result.poly.addAll(other.poly);
		result.autoFormat();
		return result;
		
	}
	
	public Polynom sub(Polynom other){ //scadere doua polinoame
		Polynom aux = new Polynom();
		for(Monom m : other.poly){
			aux.poly.add(new Monom(-m.getCoef(),m.getPower()));
		//	System.out.println(m.getCoef()+" "+m.getPower());
		}
		Polynom result=new Polynom();
		result.poly.addAll(this.poly);
		result.poly.addAll(aux.poly);
		result.autoFormat();
		result.autoFormat();
	//	System.out.println(result);
		return result;
		
		
	}
	
	public Polynom mul(Polynom other){ //inmultire doua polinoame
		Polynom result = new Polynom();
		for(Monom m : this.poly){
			for(Monom p : other.poly){
				result.poly.add(m.mul(p));
			}
		}
		result.autoFormat();
		return result;
	}
	
	public Polynom[] div(Polynom other){ //impartire doua polinoame

		Polynom deimpartit = this;
		Polynom impartitor = other;
		Polynom result = new Polynom();
		Polynom rest = new Polynom();
		Polynom[] res = new Polynom[2];
		if(!deimpartit.poly.isEmpty() && !impartitor.poly.isEmpty() && impartitor.poly.get(0).getCoef() != 0
										&& deimpartit.getGrad() >= impartitor.getGrad()	){
			while( !deimpartit.poly.isEmpty() && deimpartit.getGrad() >= impartitor.getGrad() ){
				int initialGrad = deimpartit.getGrad();
				Polynom cat = new Polynom();
				cat.poly.add(deimpartit.poly.get(0).div(impartitor.poly.get(0)));
				result.poly.add(cat.poly.get(0));
				cat = cat.mul(impartitor);
				deimpartit = deimpartit.sub(cat);
				if(!deimpartit.poly.isEmpty() && initialGrad == deimpartit.getGrad())
					deimpartit.poly.remove(0);
				//System.out.println("Deimpartit: "+deimpartit);
				//System.out.println("Impartitor: "+impartitor);
				//System.out.println("Rest: "+rest);
			}
			rest = deimpartit;
		}
		res[0]=result;
		res[1]=rest;
		//System.out.println(result);
		//System.out.println(deimpartit);
		return res;
	}
	
	public List<Monom> getPoly(){ //returnez lista de monoame
		return this.poly;
	}
	
 	
	public Polynom der(){ //operatia de derivare polinom
		Polynom result = new Polynom();
		for(Monom p : this.poly){
			result.poly.add(p.der());
		}
		Monom m=new Monom(0,0);
		for(int i = 0 ; i < result.poly.size() ; i++){
			if(result.poly.get(i).getCoef()==0)
				result.poly.set(i,m);
		}
		while(result.poly.contains(m)){
			result.poly.remove(m);
		}
		return result;
	}
	public Polynom integrate(){ //operatia de integrare polinom
		Polynom result = new Polynom();
		for(Monom m : this.poly){
			result.poly.add(m.integrate());
		}
		return result;
	}
	public boolean equals(Polynom other){ //operatia ce determina daca doua polinoame sunt egale
		boolean eq = true;
		if(this.poly.size() != other.poly.size())
			return false;
		for(int i = 0; i < this.poly.size(); i++)
				if(this.poly.get(i).getCoef() == other.poly.get(i).getCoef()
						&& this.poly.get(i).getPower() == other.poly.get(i).getPower())
					continue;
				else{
					eq = false;
					break;
				}
		return eq;	
	} 
	
	public String toString(){ //reda o forma "citibila" a polinomului
 		if(this.poly.isEmpty())
 			return "0";
		String result = new String();
		for(int i=0; i < this.poly.size() ; i++){
			if(this.poly.get(i).getCoef() < 0)
				result+= this.poly.get(i);
			else if (this.poly.get(i).getCoef() == 0) result+="";
			else{
				if( i!= 0 )
						result+= "+"+this.poly.get(i);
				else
						result+= this.poly.get(i);
			}
		}
		return result;
	}
	
 	
	public static void main(String[] args) {
		Polynom p = new Polynom();
		p.parsePoly("5x^4+4.40x^3-5.50x^2+4x+6.53");
		p.autoFormat();
		System.out.println(p);
		System.out.println("-------------------");
		Polynom p1= new Polynom();
		p1.parsePoly("5x^4+4.40x^3-5.50x^2+4x+6.53");
		p1.autoFormat();
		System.out.println(p1);
		System.out.println("-------------------");
		Polynom sum = p1.add(p);
		System.out.println(sum);
		//System.out.println("-------------------");
		//Polynom sub = p.sub(p1);
		//System.out.println(sub);
		System.out.println("-------------------");
		Polynom mul = p.mul(p1);
		System.out.println(mul);
		System.out.println("-------------------");
		Polynom test = new Polynom();
		p.div(p1);
		Polynom[] res = p.div(p1);
		System.out.println(res[0]);
		System.out.println(res[1]);
		System.out.println(p1.equals(p));

	}

}
