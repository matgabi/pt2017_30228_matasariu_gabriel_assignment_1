package polinoame;

public class PolyModel {

	private Polynom p1; //polinomul p1
	private Polynom p2; //polinom p2
	private Polynom result; //rezultatul operatiei ce se efectueaza pe p1 sau p2
	private Polynom rest; //restul operatiei ce se efectueaza asupra lui p1 sau p2
	public PolyModel(){ 
		p1 = new Polynom();
		p2 = new Polynom();
		result = new Polynom();
		rest = new Polynom();
	}
	
	public void add(){ //aduna cele 2 polinoame p1 si p2
		result = p1.add(p2);
		resetRest();
	}
	
	public void sub(){
		result = p1.sub(p2); //scade polinoamele p1 si p2
		resetRest();
	}
	
	public void mul(){ //inmulteste cele doua polinoame
		result = p1.mul(p2);
		resetRest();
	}
	
	public void div(){ //imparte cele doua polinoame
		Polynom[] res = p1.div(p2);
		result = res[0];
		rest = res[1];
		
	}
	
	public void derP1(){ //derivez primul polinom
		result = p1.der();
		resetRest();
	}
	
	public void derP2(){ //derivez al doilea polinom
		result = p2.der();
		resetRest();
	}
	
	public void intP1(){ //integrez primul polinom
		result = p1.integrate();
		resetRest();
	}
	
	public void intP2(){ //integrez al doilea polinom
		result = p2.integrate();
		resetRest();
	}
	
	
	
	public void setP1(String polynomial){ //parsez stringul ce a fost introdus in view pt p1
		p1.parsePoly(polynomial); 
	}
	
	public void setP2(String polynomial){//parsez stringul ce a fost introdus in view pt p2
		p2.parsePoly(polynomial);
	}
	
	public Polynom getP1(){ //getter p1
		return p1;
	}
	
	public Polynom getP2(){ //getter p2
		return p2;
	}
	
	public Polynom getResult(){ //getter rezultat
		return result;
	}
	
	public Polynom getRest(){ //getter rest
		return rest;
	}
	
	public void resetRest(){ //sterge toate monoamele din rest
		rest.getPoly().removeAll(rest.getPoly());
	}
	
	public void resetP1(){ //sterge practic totate monoamele din p1
		p1 = new Polynom();
	}
	
	public void resetP2(){ //sterge toate monoamele din p2
		p2 = new Polynom();
	}
	

	
}
