package polinoame;

import static org.junit.Assert.*;
import org.junit.*;

public class JUnitTest {

	private static Polynom p1;
	private static Polynom p2;

	public JUnitTest() {
		System.out.println("Constructor inaintea fiecarui test!");
	}

	@BeforeClass
	public static void setUpBeforeClass() {
		System.out.println("O singura data inaintea executiei setului de teste din clasa!");
		p1 = new Polynom();
		p2 = new Polynom();
		p1.parsePoly("5x^4+4.4x^3-12x^2+7x+3.41");
		p2.parsePoly("6.5x^2-3x+3.12");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("O singura data dupa terminarea executiei setului de teste din clasa!");
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("Incepe un nou test!");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("S-a terminat testul curent!");
	}

	@Test
	public void testSum() { // testez suma
		String t = p1.add(p2).toString();
		assertEquals(t, "5x^4 + 4.40x^3-5.50x^2 + 4x + 6.53");
	}

	@Test
	public void testDif() { // testez diferenta
		String t = p1.sub(p2).toString();
		assertEquals(t, "5x^4 + 4.40x^3-18.50x^2 + 10x + 0.29");
	}

	@Test
	public void testMul() { // testez inmultirea
		String t = p1.mul(p2).toString();
		assertEquals(t, "32.50x^6 + 13.60x^5-75.60x^4 + 95.23x^3-36.27x^2 + 11.61x + 10.64");
	}

	@Test
	public void testDiv() {// testez impartirea
		String result = p1.div(p2)[0].toString();
		String rest = p1.div(p2)[1].toString();
		assertEquals(result, "0.77x^2 + 1.03x-1.74");
		assertEquals(rest, "-1.44x + 8.84");
	}

	@Test
	public void testDerP1() { // testez derivarea
		String t = p1.der().toString();
		assertEquals(t, "20x^3 + 13.20x^2-24x + 7");
	}

	@Test
	public void testDerP2() { // testez derivarea
		String t = p2.der().toString();
		assertEquals(t, "13x-3");
	}

	@Test
	public void testIntP1() { // testez integrarea
		String t = p1.integrate().toString();
		assertEquals(t, "x^5 + 1.10x^4-4x^3 + 3.50x^2 + 3.41x");
	}

	@Test
	public void testIntP2() { // testez integrarea
		String t = p2.integrate().toString();
		assertEquals(t, "2.17x^3-1.50x^2 + 3.12x");
	}

}
